<?php

namespace App\Http\Livewire\Backend\Produk;

use App\Models\Produk;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\KategoriProduk;
use App\Models\Brand;
use App\Models\User;

class FormProduk extends Component
{
    use WithFileUploads;
    public Produk $produk;
    public $gambar, $kategori, $brand, $user;

    protected $rules = [
        'produk.title'        => 'required',
        'produk.excerpt'      => 'required',
        'produk.slug'         => 'required',
        'produk.deskripsi'      => '',
        'produk.price'        => '',
        'produk.price_disc'   => '',
        'produk.kategori_id'  => 'required',
        'produk.brand_id'  => '',
        'produk.users_id'  => '',
    ];

    public function mount($id = null)
    {

        $this->produk = new Produk();
        if ($id) {
            $this->produk = Produk::findOrFail($id);
        }

        $this->kategori = KategoriProduk::get();
        $this->brand = Brand::get();
        $this->user = User::whereHas('roles', function ($query) {
            $query->where('id', 2); // Menambahkan kriteria filter untuk role_id = 2
        })->get();
    }

    public function render()
    {
        return view('livewire.backend.produk.form-produk');
    }

    public function updatedProduk($value, $key)
    {
        if ($key == 'title') {
            $this->produk->slug = Str::slug($value);
            $this->validateOnly('produk.slug');
        }
    }

    public function save()
    {
        if ($this->produk['brand_id'] === '') {
            $this->produk['brand_id'] = null;
        }
        if ($this->produk['users_id'] === '') {
            $this->produk['users_id'] = null;
        }
        $this->validate();
        if (!$this->produk->image) {
            $this->validate([
                'gambar'    => 'required|image|mimes:jpg,jpeg,png,webp|max:250',
            ]);
        }
        $this->validate([
            'produk.slug' => 'required|unique:produk,slug,' . $this->produk->id,
        ]);

        if ($this->gambar) {            
            $gambarPath = $this->gambar->store('sanitary/produk', 's3');
            $this->produk->image = $gambarPath;      
        }
        try {
            $this->produk->save();
            $this->dispatchBrowserEvent('success-izi', ['ntitle' => 'Success', 'nmessage' => "Produk berhasil ditambahkan"]);

            redirect(route('data-produk'));
        } catch (\Throwable $th) {
            //throw $th;
            Log::error($th);
            $this->dispatchBrowserEvent('error-izi', ['ntitle' => 'Error', 'nmessage' => "Produk tidak berhasil ditambahkan"]);
        }
    }
}
