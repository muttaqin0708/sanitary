<?php

namespace App\Http\Livewire\Backend\Produk;

use App\Models\Produk;
use Livewire\Component;

class DetailProduk extends Component
{
    public $produk;
    public function mount($slug)
    {
        $this->produk = Produk::where('slug', $slug)->firstOrFail();
    }

    public function render()
    {
        return view('livewire.backend.produk.detail-produk');
    }
}
