<?php

namespace App\Http\Livewire\Backend\TopProduk;

use App\Models\TopProduk;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Illuminate\Support\Facades\Storage;

class PublishedTopProduk extends DataTableComponent
{
    protected $listeners = ['refresh' => '$refresh'];
    
    public function columns(): array
    {
        return [
            Column::make('Top Produk', 'data_produk.title')->searchable()->format(function ($value, $column, $row) {
                return "
                $value
                <div class='table-links'>
                             <a taret='_blank' href='" . route('detail-top-produk', $row->id) . "'>View</a>
                              <div class='bullet'></div>
                              <a href='" . route('edit-top-produk', $row->id) . "'>Edit</a>
                              <div class='bullet'></div>
                              <a href='#' class='text-danger btnAction' role='button' data-action='confirm' data-id='$row->id' data-force='false'>Hapus</a>
                            </div>
                            ";
            })->asHtml()->addClass('col-12'),
        ];
    }

    public function query(): Builder
    {
        return TopProduk::query()->with('data_produk')->orderBy('created_at', 'desc'); 
    }
}
