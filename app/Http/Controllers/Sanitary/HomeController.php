<?php

namespace App\Http\Controllers\Sanitary;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\KategoriProduk;
use App\Models\Produk;

class HomeController extends Controller
{
    public function index()
    {
        $category = KategoriProduk::get();
        $latestBlogs = Blog::where('status', 'Publish')->latest()->take(3)->get();
        $latestProduct = Produk::latest()->take(6)->get();
        $newProducts= Produk::latest()->take(6)->get();
        return view('sanitary.home.index', compact('latestBlogs', 'category', 'newProducts', 'latestProduct'));
    }

}
