<?php

namespace App\Http\Controllers\Sanitary;

use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index()
    {
        return view('sanitary.home.about');
    }

}
