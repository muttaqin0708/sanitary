<?php

namespace App\Http\Controllers\Sanitary;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\KategoriBlog;


class BlogController extends Controller
{
    public function index()
    {
        $blog = Blog::where('status', 'Publish')->paginate(10);
        $latestBlogs = Blog::where('status', 'Publish')->latest()->take(3)->get();
        $category = KategoriBlog::get();
        return view('sanitary.home.blog-list', compact('blog', 'latestBlogs', 'category'));
    }

    public function show($id)
    {
        $blogDetail = Blog::find($id);
        $latestBlogs = Blog::latest()->take(3)->get();
        $category = KategoriBlog::get();
        return view('sanitary.home.blog-detail',compact('latestBlogs', 'blogDetail', 'category'), (['blogDetail' => $blogDetail]));
    }
}
