<?php

namespace App\Http\Controllers\Sanitary;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\KategoriProduk;
use App\Models\Produk;
use App\Models\TopProduk;

class ProductController extends Controller
{
    public function index()
    {
        $produk = Produk::paginate(12);
        $category = KategoriProduk::get();
        $latestProduct = Produk::latest()->take(3)->get();
        $top = TopProduk::get();
        $popularBrands = Brand::get();
        return view('sanitary.product.product-list', compact('produk', 'category', 'latestProduct','top', 'popularBrands'));
    }

    public function detail()
    {
        return view('sanitary.product.detail.index');
    }

    public function show($id)
    {
        $product = Produk::with('sales_produk')->find($id);
        // dd($product->sales_produk->telp);

        return view('sanitary.product.detail.index', [
            'product' => $product,
            'nohp'=> $product->sales_produk->telp
        ]);
    }
}
