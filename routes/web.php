<?php

use App\Http\Controllers\Backend\Area;
// lulu
use App\Http\Controllers\Sanitary\AboutController;
use App\Http\Controllers\Sanitary\BlogController;
use App\Http\Controllers\Sanitary\ContactController;
use App\Http\Controllers\SitemapController;
use App\Http\Controllers\Sanitary\HomeController;
use App\Http\Controllers\Sanitary\ProductController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/link', function () {
    Artisan::call('cache:clear');
    Artisan::call('storage:link');
});

Route::get('/', [HomeController::class, 'index'])->name('homepage');

//lulu


Route::get('/about', [AboutController::class, 'index'])->name('about');


Route::get('/blog-list', [BlogController::class, 'index'])->name('blog');
Route::get('/blog-detail/{id}', [BlogController::class, 'show'])->name('blog.detail');;

Route::get('/contact', [ContactController::class, 'index'])->name('contact');

/*
batas suci
*/

Route::get('/sitemap.xml', [SitemapController::class, 'index']);

//muttaqin

Route::get('/product', [ProductController::class, 'index'])->name('product');
Route::get('/product-detail', [ProductController::class, 'detail']);
Route::get('/product-detail/{id}', [ProductController::class, 'show'])->name('product.detail');
Route::post('/product-detail', [ProductController::class, 'create']);

//backend routes
Route::middleware(['auth:web', 'verified'])->prefix('dashboard')->group(function () {
    Route::get('/', \App\Http\Livewire\Dashboard::class)->name('dashboard');


    Route::prefix('user')->group(function () {
        Route::get('/', \App\Http\Livewire\User\User::class)->middleware(['can:users'])->name('user');
        Route::get('create', \App\Http\Livewire\User\Form\FormUser::class)->middleware(['can:users.create'])->name('createuser');
        Route::get('edit/{id}', \App\Http\Livewire\User\Form\FormUser::class)->middleware(['can:users.edit'])->name('edituser');


        Route::get('roles', \App\Http\Livewire\User\Roles::class)->middleware(['can:roles'])->name('roles');
        Route::get('permissions', \App\Http\Livewire\Permissions::class)->middleware(['can:permissions'])->name('permissions');
        Route::get('info/{id?}', \App\Http\Livewire\User\UserProfile::class)->name('user-profile');
    });



    Route::prefix('blog')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Blog\Blogs::class)->name('data-blog');
        Route::get('/create', App\Http\Livewire\Backend\Blog\FormBlog::class)->name('create-blog');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Blog\FormBlog::class)->name('edit-blog');
        Route::get('/{slug}.html', App\Http\Livewire\Backend\Blog\DetailBlog::class)->name('detail-blog');

        // Route::get('/{slug}.html', App\Http\Livewire\Frontend\Blog\DetailBlog::class)->name('detail-blog');
    });

    Route::prefix('kategori-blog')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\KategoriBlog\KategoriBlog::class)->name('data-kategori-blog');
        Route::get('/create', App\Http\Livewire\Backend\KategoriBlog\FormKategoriBlog::class)->name('create-kategori-blog');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\KategoriBlog\FormKategoriBlog::class)->name('edit-kategori-blog');
        Route::get('/{slug}.html', App\Http\Livewire\Backend\KategoriBlog\DetailKategoriBlog::class)->name('detail-kategori-blog');
    });


    //start - syukron488@gmail.com
    Route::prefix('tag')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Tag\Tags::class)->name('data-tag');
        Route::get('/create', App\Http\Livewire\Backend\Tag\FormTag::class)->name('create-tag');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Tag\FormTag::class)->name('edit-tag');
        Route::get('/{id}.html', App\Http\Livewire\Backend\Tag\DetailTag::class)->name('detail-tag');
    });

    Route::prefix('gtagmanager')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\GtagManager\GtagManager::class)->name('data-gtagmanager');
        Route::get('/create', App\Http\Livewire\Backend\GtagManager\FormGtagManager::class)->name('create-gtagmanager');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\GtagManager\FormGtagManager::class)->name('edit-gtagmanager');
        Route::get('/{id}.html', App\Http\Livewire\Backend\GtagManager\DetailGtagManager::class)->name('detail-gtagmanager');
    });

    Route::prefix('analytics')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Analytics\Analytics::class)->name('data-analytics');
        Route::get('/create', App\Http\Livewire\Backend\Analytics\FormAnalytics::class)->name('create-analytics');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Analytics\FormAnalytics::class)->name('edit-analytics');
        Route::get('/{id}.html', App\Http\Livewire\Backend\Analytics\DetailAnalytics::class)->name('detail-analytics');
    });
    //end - syukron488@gmail.com

    Route::prefix('brand')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Brand\Brand::class)->name('data-brand');
        Route::get('/create', App\Http\Livewire\Backend\Brand\FormBrand::class)->name('create-brand');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Brand\FormBrand::class)->name('edit-brand');
        Route::get('/{slug}.html', App\Http\Livewire\Backend\Brand\DetailBrand::class)->name('detail-brand');
    });

    Route::prefix('produk')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Produk\Produk::class)->name('data-produk');
        Route::get('/create', App\Http\Livewire\Backend\Produk\FormProduk::class)->name('create-produk');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Produk\FormProduk::class)->name('edit-produk');
        Route::get('/{slug}.html', App\Http\Livewire\Backend\Produk\DetailProduk::class)->name('detail-produk');
    });

    Route::prefix('top-produk')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\TopProduk\TopProduk::class)->name('data-top-produk');
        Route::get('/create', App\Http\Livewire\Backend\TopProduk\FormTopProduk::class)->name('create-top-produk');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\TopProduk\FormTopProduk::class)->name('edit-top-produk');
        Route::get('/{id}.html', App\Http\Livewire\Backend\TopProduk\DetailTopProduk::class)->name('detail-top-produk');
    });

    Route::prefix('kategori-produk')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\KategoriProduk\KategoriProduk::class)->name('data-kategori-produk');
        Route::get('/create', App\Http\Livewire\Backend\KategoriProduk\FormKategoriProduk::class)->name('create-kategori-produk');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\KategoriProduk\FormKategoriProduk::class)->name('edit-kategori-produk');
        Route::get('/{slug}.html', App\Http\Livewire\Backend\KategoriProduk\DetailKategoriProduk::class)->name('detail-kategori-produk');
    });

    Route::prefix('history-pesanan')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\HistoryPesanan\HistoryPesanan::class)->name('history-pesanan');
        Route::get('/{id}', App\Http\Livewire\Backend\HistoryPesanan\DetailHistoryPesanan::class)->name('detail-history-pesanan');
    });

    Route::prefix('faq')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Faq\Faqs::class)->name('data-faq');
        Route::get('/create', App\Http\Livewire\Backend\Faq\FormFaq::class)->name('create-faq');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Faq\FormFaq::class)->name('edit-faq');
        Route::get('/{id}', App\Http\Livewire\Backend\Faq\DetailFaq::class)->name('detail-faq');
    });

    Route::prefix('feedback')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Feedback\Feedback::class)->name('data-feedback');
        Route::get('/create', App\Http\Livewire\Backend\Feedback\FormFeedback::class)->name('create-feedback');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Feedback\FormFeedback::class)->name('edit-feedback');
        Route::get('/{id}', App\Http\Livewire\Backend\Feedback\DetailFeedback::class)->name('detail-feedback');
    });

    Route::prefix('page')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Page\Pages::class)->name('data-page');
        Route::get('/create', App\Http\Livewire\Backend\Page\FormPage::class)->name('create-page');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Page\FormPage::class)->name('edit-page');
        Route::get('/{slug}.html', App\Http\Livewire\Backend\Page\DetailPage::class)->name('detail-page');
    });

    Route::prefix('parent-area')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\ParentArea\ParentArea::class)->name('data-parent-area');
        Route::get('/create', App\Http\Livewire\Backend\ParentArea\FormParentArea::class)->name('create-parent-area');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\ParentArea\FormParentArea::class)->name('edit-parent-area');
        Route::get('/{slug}.html', App\Http\Livewire\Backend\ParentArea\DetailParentArea::class)->name('detail-parent-area');
    });

    Route::prefix('area')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Area\Area::class)->name('data-area');
        Route::get('/create', App\Http\Livewire\Backend\Area\FormArea::class)->name('create-area');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Area\FormArea::class)->name('edit-area');
        Route::get('/{id}', App\Http\Livewire\Backend\Area\DetailArea::class)->name('detail-area');
    });

    Route::prefix('kontak')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Kontak\Kontak::class)->name('data-kontak');
        Route::get('/create', App\Http\Livewire\Backend\Kontak\FormKontak::class)->name('create-kontak');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Kontak\FormKontak::class)->name('edit-kontak');
        Route::get('/{id}', App\Http\Livewire\Backend\Kontak\DetailKontak::class)->name('detail-kontak');
    });

    Route::prefix('link-youtube')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Youtube\Youtube::class)->name('data-youtube');
        Route::get('/create', App\Http\Livewire\Backend\Youtube\FormYoutube::class)->name('create-youtube');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Youtube\FormYoutube::class)->name('edit-youtube');
        Route::get('/{id}', App\Http\Livewire\Backend\Youtube\DetailYoutube::class)->name('detail-youtube');
    });

    Route::prefix('carousel')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Carousel\Carousel::class)->name('data-carousel');
        Route::get('/create', App\Http\Livewire\Backend\Carousel\FormCarousel::class)->name('create-carousel');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Carousel\FormCarousel::class)->name('edit-carousel');
        Route::get('/{id}', App\Http\Livewire\Backend\Carousel\DetailCarousel::class)->name('detail-carousel');
    });

    Route::prefix('gallery')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\Gallery\Gallery::class)->name('data-gallery');
        Route::get('/create', App\Http\Livewire\Backend\Gallery\FormGallery::class)->name('create-gallery');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\Gallery\FormGallery::class)->name('edit-gallery');
        Route::get('/{id}', App\Http\Livewire\Backend\Gallery\DetailGallery::class)->name('detail-gallery');
    });

    Route::prefix('search-console')->group(function () {
        Route::get('/', App\Http\Livewire\Backend\SearchConsole\SearchConsole::class)->name('data-search-console');
        // Route::get('/create', App\Http\Livewire\Backend\Tag\FormTag::class)->name('create-tag');
        Route::get('/edit/{id}', App\Http\Livewire\Backend\SearchConsole\FormSearchConsole::class)->name('edit-search-console');
        Route::get('/{id}.html', App\Http\Livewire\Backend\SearchConsole\DetailSearchConsole::class)->name('detail-search-console');
    });
    // syukron488@gmail.com

    Route::get('config', \App\Http\Livewire\Config::class)->name('config');

    Route::get('clearnotif', function () {
        return markNotificationAsRead();
    })->name('clearnotif');

    Route::get('clear', function () {
        Artisan::call('cache:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');
        Artisan::call('optimize');
        return "all clear";
    })->role('Superadmin');
});
