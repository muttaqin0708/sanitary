@extends('sanitary.layout.app')
@section('content')

<main>
    <!-- blog details area start -->
    <section class="tp-postbox-details-area pb-120 pt-95">
       <div class="container">

          <div class="row">
             {{-- @foreach ($blog as $item) --}}
             <div class="col-xl-9">

                <div class="tp-postbox-details-top">
                   <div class="tp-postbox-details-category">
                      <span>
                         <a href="#">{{ $blogDetail->keywoard }}</a>
                      </span>
                      <span>
                         <a href="#">{{ $blogDetail->keywoard }}</a>
                      </span>
                   </div>
                   <h3 class="tp-postbox-details-title">{{ $blogDetail->title }}</h3>
                   <div class="tp-postbox-details-meta mb-50">
                      <span data-meta="author">
                         <svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7.4104 8C9.33922 8 10.9028 6.433 10.9028 4.5C10.9028 2.567 9.33922 1 7.4104 1C5.48159 1 3.91797 2.567 3.91797 4.5C3.91797 6.433 5.48159 8 7.4104 8Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M13.4102 15.0001C13.4102 12.2911 10.721 10.1001 7.41016 10.1001C4.09933 10.1001 1.41016 12.2911 1.41016 15.0001" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                         </svg>
                         By <a href="#">TraoStudio</a>
                      </span>
                      <span>
                         <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15 8.5C15 12.364 11.864 15.5 8 15.5C4.136 15.5 1 12.364 1 8.5C1 4.636 4.136 1.5 8 1.5C11.864 1.5 15 4.636 15 8.5Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M10.5972 10.7259L8.42721 9.43093C8.04921 9.20693 7.74121 8.66793 7.74121 8.22693V5.35693" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                         </svg>
                         {{ $blogDetail->created_at }}
                        </span>
                   </div>
                </div>

            </div>
             <div class="col-xl-12">
                <div class="tp-postbox-details-thumb">
                   <img src="{{ Storage::disk('s3')->url($blogDetail->image) }}" alt="">
                </div>
             </div>
          </div>
          <div class="row">
             <div class="col-xl-9 col-lg-8">
                <div class="tp-postbox-details-main-wrapper">
                   <div class="tp-postbox-details-content">
                      <p class="tp-dropcap">{{ $blogDetail->content }}</p>
                      <div class="tp-postbox-details-share-wrapper">
                         <div class="row">
                            <div class="col-xl-8 col-lg-6">
                               <div class="tp-postbox-details-tags tagcloud">
                                  <span>Tags:</span>
                                  <a href="#">Lifesttyle</a>
                                  <a href="#">Awesome</a>
                                  <a href="#">Winter</a>
                                  <a href="#">Sunglasses</a>
                               </div>
                            </div>
                            <div class="col-xl-4 col-lg-6">
                               <div class="tp-postbox-details-share text-md-end">
                                  <span>Share:</span>
                                  <a href="#"><i class="fa-brands fa-facebook-f"></i></a>
                                  <a href="#"><i class="fa-brands fa-twitter"></i></a>
                                  <a href="#"><i class="fa-brands fa-linkedin-in"></i></a>
                               </div>
                            </div>
                         </div>
                      </div>

                      <div class="tp-postbox-details-author d-sm-flex align-items-start" data-bg-color="#F4F7F9">
                         <div class="tp-postbox-details-author-thumb">
                            <a href="#">
                               <img src="assets/img/users/user-11.jpg" alt="">
                            </a>
                         </div>
                         <div class="tp-postbox-details-author-content">
                            <span>Written by</span>
                            <h5 class="tp-postbox-details-author-title">
                               <a href="#">Theodore Handle</a>
                            </h5>
                            <p>By defining and following internal and external processes, your team will have clarity on resources to attract and retain customers for your business.</p>

                            <div class="tp-postbox-details-author-social">
                               <a href="#"><i class="fa-brands fa-facebook-f"></i></a>
                               <a href="#"><i class="fa-brands fa-twitter"></i></a>
                               <a href="#"><i class="fa-brands fa-linkedin-in"></i></a>
                               <a href="#"><i class="fa-brands fa-vimeo-v"></i></a>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <div class="col-xl-3 col-lg-4">
                <div class="tp-sidebar-wrapper tp-sidebar-ml--24">
                   <div class="tp-sidebar-widget mb-35">
                      <div class="tp-sidebar-search">
                         <form action="#">
                            <div class="tp-sidebar-search-input">
                               <input type="text" placeholder="Search...">
                               <button type="submit">
                                  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                     <path d="M8.11111 15.2222C12.0385 15.2222 15.2222 12.0385 15.2222 8.11111C15.2222 4.18375 12.0385 1 8.11111 1C4.18375 1 1 4.18375 1 8.11111C1 12.0385 4.18375 15.2222 8.11111 15.2222Z" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                     <path d="M16.9995 17L13.1328 13.1333" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                  </svg>
                               </button>
                            </div>
                         </form>
                      </div>
                   </div>

                   <!-- about -->
                   <div class="tp-sidebar-widget mb-35">
                    <h3 class="tp-sidebar-widget-title">About Us</h3>
                    <div class="tp-sidebar-widget-content">
                        <div class="tp-sidebar-about">
                            <div class="tp-sidebar-about-thumb mb-25">
                                <a href="{{ route('about') }}">
                                    <img src="{{ asset('img/logo_sanitary.png') }}" alt="">
                                </a>
                            </div>
                            <div class="tp-sidebar-about-content">
                                <h3 class="tp-sidebar-about-title">
                                    <a href="{{ route('about') }}">RUMAH SANITARY</a>
                                </h3>
                                <p>Rumah sanitary merupakan direktori bisnis yang menyediakan berbagai macam kebutuhan sanitary lengkap dan berkualitas seperti kran air, shower, solar water heater, electric waterheater, dan lain-lain.</p>
                            </div>
                        </div>
                    </div>
                </div>
                   <!-- about end -->

                   <!-- latest post start -->
                   <div class="tp-sidebar-widget mb-35">
                      <h3 class="tp-sidebar-widget-title">Latest Posts</h3>
                      <div class="tp-sidebar-widget-content">
                         <div class="tp-sidebar-blog-item-wrapper">
                            @foreach ($latestBlogs as $post)
                            <div class="tp-sidebar-blog-item d-flex align-items-center">
                               <div class="tp-sidebar-blog-thumb">
                                <a href="blog-detail/{{$post->id}}">
                                     <img src="{{ Storage::disk('s3')->url($post->image) }}" alt="">
                                  </a>
                               </div>
                               <div class="tp-sidebar-blog-content">
                                  <div class="tp-sidebar-blog-meta">
                                     <span>{{ $post->created_at->format('d F, Y') }}</span>
                                  </div>
                                  <h3 class="tp-sidebar-blog-title">
                                     <a href="blog-details.html">{{ $post->title }}</a>
                                  </h3>
                               </div>
                            </div>
                             @endforeach
                        </div>
                      </div>
                   </div>
                   <!-- latest post end -->

                   <!-- categories start -->
                   <div class="tp-sidebar-widget widget_categories mb-35">
                      <h3 class="tp-sidebar-widget-title">Categories</h3>
                      <div class="tp-sidebar-widget-content">
                        @foreach ($category as $item)
                        <ul>
                            <li><a href="#">{{ $item->title }}<span</span></a></li>
                        </ul>
                        @endforeach
                      </div>
                   </div>
                   <!-- categories end -->

                   <!-- tag cloud start -->
                   <div class="tp-sidebar-widget mb-35">
                      <h3 class="tp-sidebar-widget-title">Popular Tags</h3>
                      <div class="tp-sidebar-widget-content tagcloud">
                         <a href="#">Summer</a>
                         <a href="#">Vintage</a>
                         <a href="#">Sunglasses</a>
                         <a href="#">Organic Food</a>
                         <a href="#">Lifesttyle</a>
                         <a href="#">Nature</a>
                      </div>
                   </div>
                   <!-- tag cloud end -->
                </div>
             </div>
          </div>
        </div>
    </section>
    <!-- blog details area end -->
 </main>

 @endsection
