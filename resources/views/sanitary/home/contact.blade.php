@extends('sanitary.layout.app')
@section('content')

<main>
    <!-- breadcrumb area start -->
    <section class="breadcrumb__area include-bg text-center pt-95 pb-50">
       <div class="container">
          <div class="row">
             <div class="col-xxl-12">
                <div class="breadcrumb__content p-relative z-index-1">
                   <h3 class="breadcrumb__title">Keep In Touch with Us</h3>
                </div>
             </div>
          </div>
       </div>
    </section>
    <!-- breadcrumb area end -->


    <!-- contact area start -->
    <section class="tp-contact-area pb-100">
       <div class="container">
          <div class="tp-contact-inner">
             <div class="row">
                <div class="col-xl-9 col-lg-8">
                   <div class="tp-contact-wrapper">
                      <h3 class="tp-contact-title">Sent A Message</h3>

                      <div class="tp-contact-form">
                         <form id="contact-form" action="assets/mail.php" method="POST">
                            <div class="tp-contact-input-wrapper">
                               <div class="tp-contact-input-box">
                                  <div class="tp-contact-input">
                                     <input name="name" id="name" type="text">
                                  </div>
                                  <div class="tp-contact-input-title">
                                     <label for="name">Your Name</label>
                                  </div>
                               </div>
                               <div class="tp-contact-input-box">
                                  <div class="tp-contact-input">
                                     <input name="email" id="email" type="email">
                                  </div>
                                  <div class="tp-contact-input-title">
                                     <label for="email">Your Email</label>
                                  </div>
                               </div>
                               <div class="tp-contact-input-box">
                                  <div class="tp-contact-input">
                                     <input name="subject" id="subject" type="text">
                                  </div>
                                  <div class="tp-contact-input-title">
                                     <label for="subject">Subject</label>
                                  </div>
                               </div>
                               <div class="tp-contact-input-box">
                                  <div class="tp-contact-input">
                                    <textarea id="message" name="message" placeholder="Write your message here..."></textarea>
                                  </div>
                                  <div class="tp-contact-input-title">
                                     <label for="message">Your Message</label>
                                  </div>
                               </div>
                            </div>
                            <div class="tp-contact-suggetions mb-20">
                               <div class="tp-contact-remeber">
                                  <input id="remeber" type="checkbox">
                                  <label for="remeber">Save my name, email, and website in this browser for the next time I comment.</label>
                               </div>
                            </div>
                            <div class="tp-contact-btn">
                               <button type="submit">Send Message</button>
                            </div>
                         </form>
                         <p class="ajax-response"></p>
                      </div>
                   </div>
                </div>
                <div class="col-xl-3 col-lg-4">
                   <div class="tp-contact-info-wrapper">
                      <div class="tp-contact-info-item">
                         <div class="tp-contact-info-icon">
                            <span>
                               <img src="assets/img/contact/contact-icon-1.png" alt="">
                            </span>
                         </div>
                         <div class="tp-contact-info-content">
                            <p data-info="mail"><a href="mailto:sanitaryrumahhh@gmail.com">rumahsanitary@gmail.com</a></p>
                            <p data-info="phone"><a href="tel:+6285959512435">+(62) 859 5951 2435</a></p>
                         </div>
                      </div>
                      <div class="tp-contact-info-item">
                         <div class="tp-contact-info-icon">
                            <span>
                               <img src="assets/img/contact/contact-icon-2.png" alt="">
                            </span>
                         </div>
                         <div class="tp-contact-info-content">
                            <p>
                               <a href="https://maps.app.goo.gl/LB95q7n3kPmjS92fA" target="_blank">
                                Komp. Ruko Mayor Oking II,<br>Jl Raya Mayor Oking Jaya Atmaja 8 <br> Kab. Bogor, Jawa Barat 16918
                               </a>
                            </p>
                         </div>
                      </div>
                      <div class="tp-contact-info-item">
                         <div class="tp-contact-info-icon">
                            <span>
                               <img src="assets/img/contact/contact-icon-3.png" alt="">
                            </span>
                         </div>
                         <div class="tp-contact-info-content">
                            <div class="tp-contact-social-wrapper mt-5">
                               <h4 class="tp-contact-social-title">Find on social media</h4>

                               <div class="tp-contact-social-icon">
                                  <a href="#"><i class="fa-brands fa-facebook-f"></i></a>
                                  <a href="#"><i class="fa-brands fa-twitter"></i></a>
                                  <a href="#"><i class="fa-brands fa-linkedin-in"></i></a>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </section>
    <!-- contact area end -->
 </main>
 @endsection
